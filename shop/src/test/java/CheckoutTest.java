import Base.BaseTest;
import Models.Order;
import Models.User;
import Pages.Checkout.CheckoutBasketPage;
import Pages.Checkout.CheckoutInfoPage;
import Pages.Checkout.CheckoutResultsPage;
import Pages.ListOfProductsPage;
import Pages.MenuPage;
import Pages.ProductPage;
import Providers.UserFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class CheckoutTest extends BaseTest {
    Order order;
    UserFactory userFactory;
    User user;
    ProductPage productPage;
    ListOfProductsPage listOfProductsPage;
    MenuPage menuPage;
    CheckoutBasketPage checkoutBasketPage;
    CheckoutInfoPage checkoutInfoPage;
    CheckoutResultsPage checkoutResultsPage;

    @BeforeMethod
    public void prepareForTest() {
        order = new Order();
        userFactory = new UserFactory();
        user = userFactory.getKowalski();
        productPage = new ProductPage(driver);
        listOfProductsPage = new ListOfProductsPage(driver);
        menuPage = new MenuPage(driver);
        checkoutBasketPage = new CheckoutBasketPage(driver);
        checkoutInfoPage = new CheckoutInfoPage(driver);
        checkoutResultsPage = new CheckoutResultsPage(driver);
    }

    @Test
    public void checkoutTest() throws InterruptedException {
        driver.get("http://store.demoqa.com/products-page/product-category/accessories/");
        listOfProductsPage.openProduct("Magic Mouse");

        productPage.addToBasket(order);
        menuPage.waitForCount(order.getAmountOfProducts());

        productPage.addToBasket(order);
        menuPage.waitForCount(order.getAmountOfProducts());

        driver.get("http://store.demoqa.com/products-page/product-category/accessories/");
        listOfProductsPage.openProduct("Apple TV");

        productPage.addToBasket(order);
        menuPage.waitForCount(order.getAmountOfProducts());

        menuPage.goToBasket();

        checkoutBasketPage.waitForListOfProducts(order.getProducts().size());
        checkoutBasketPage.verifyOrder(order);
        checkoutBasketPage.goToStep2();

        checkoutInfoPage.setShippingAddressSameAsBilling();
        checkoutInfoPage.setUserData(user);
        checkoutInfoPage.setAddressData(user);
        checkoutInfoPage.purchase();

        checkoutResultsPage.waitForResultPage();

        System.out.println("Total order cost: " + order.getTotalOrderCost());
        System.out.println("Total amount od products: " + order.getAmountOfProducts());
    }
}
