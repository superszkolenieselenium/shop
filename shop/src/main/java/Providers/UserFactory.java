package Providers;

import Models.User;

public class UserFactory {

    public User getKowalski(){
        return new User("Jan", "Kowalski",
                "3Maja", "Warszawa", "Poland",
        "321654987", "wtf?");
    }

    public User getNowak(){
        return new User("Adam", "Nowak",
                "Dusigrosza", "Poznan", "Poland",
                "123456789", "wtf???");
    }
}
