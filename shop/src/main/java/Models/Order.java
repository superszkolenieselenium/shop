package Models;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Order {
    private List<Product> products = new ArrayList<Product>();

    public List<Product> getProducts() {
        return products;
    }

    public Order() {
    }

    public void addNewProduct(String name, BigDecimal price){
        for (Product product: products) {
            if(product.getName().equals(name)){
                product.addOneProduct();
                return;
            }
        }
        products.add(new Product(name,price));
    }

    public BigDecimal getTotalOrderCost() {
        BigDecimal totalOrderCost = new BigDecimal(0);
        for (Product product : products) {
            totalOrderCost = totalOrderCost.add(product.getTotalCostOfProduct());
        }
        return totalOrderCost;
    }

    public int getAmountOfProducts(){
        int amountOfProducts = 0;
        for (Product product : products) {
            amountOfProducts += product.getAmount();
        }
        return amountOfProducts;
    }
}
