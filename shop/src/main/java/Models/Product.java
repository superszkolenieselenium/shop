package Models;

import java.math.BigDecimal;

public class Product {
    private String name;
    private BigDecimal price;
    int amount;

    public Product(String name, BigDecimal price) {
        this.name = name;
        this.price = price;
        this.amount = 1;
    }

    public void addOneProduct() {
        amount++;
    }

    public BigDecimal getTotalCostOfProduct() {
        return price.multiply(new BigDecimal(amount));
    }

    //gettery i settery
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
