package Pages.Base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class BasePage {
    public WebDriver driver;
    public Actions actionBuilder;
    public WebDriverWait wait;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        actionBuilder = new Actions(driver);
        wait = new WebDriverWait(driver, 20);
    }

    public void waitForElement(WebElement element) {
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public void waitForText(WebElement element, String text) {
        wait.until(ExpectedConditions.
                textToBePresentInElement(element, text));
    }


}
