package Pages.Checkout;

import Models.Order;
import Pages.Base.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

import java.math.BigDecimal;
import java.util.List;

public class CheckoutBasketPage extends BasePage {

    @FindBy(className = "pricedisplay")
    private WebElement totalPrice;

    @FindBy(css = ".product_row ")
    private List<WebElement> listOfProducts;

    @FindBy(className = "step2")
    private WebElement toStep2Button;

    public CheckoutBasketPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void goToStep2(){
        toStep2Button.click();
    }
    public void waitForListOfProducts(int amountOfProductsToBeDisplayed) {
        wait.until(ExpectedConditions.numberOfElementsToBe(By.cssSelector(".product_row"),
                amountOfProductsToBeDisplayed));
    }

    public void verifyOrder(Order order) {
        for (int i = 0; i < listOfProducts.size(); i++) {
            String actualProductDetails = listOfProducts.get(i).getText();

            String expectedName = order.getProducts().get(i).getName();
            BigDecimal expectedPrice = order.getProducts().get(i).getPrice();
            BigDecimal expectedTotalPrice = order.getProducts().get(i).getTotalCostOfProduct();

            Assert.assertTrue(actualProductDetails.contains(expectedName));
            Assert.assertTrue(actualProductDetails.contains(expectedPrice.toString()));
            Assert.assertTrue(actualProductDetails.contains(expectedTotalPrice.toString()));
        }
    }
}
