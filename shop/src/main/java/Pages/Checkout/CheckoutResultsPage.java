package Pages.Checkout;

import Pages.Base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CheckoutResultsPage  extends BasePage {
    public CheckoutResultsPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver,this);
    }

    @FindBy(className = "wpsc-purchase-log-transaction-results")
    private WebElement resultTable;

    public void waitForResultPage(){
        waitForElement(resultTable);
    }
}
