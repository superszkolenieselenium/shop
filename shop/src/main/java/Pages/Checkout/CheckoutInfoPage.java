package Pages.Checkout;

import Models.User;
import Pages.Base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class CheckoutInfoPage extends BasePage {
    public CheckoutInfoPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "input[title='billingfirstname']")
    private WebElement firstName;

    @FindBy(css = "input[value='Purchase']")
    private WebElement purchase;

    @FindBy(css = "input[title='billinglastname']")
    private WebElement lastName;

    @FindBy(css = "textarea[title='billingaddress']")
    private WebElement address;

    @FindBy(css = "input[title='billingstate']")
    private WebElement undefined;

    @FindBy(css = "input[title='billingphone']")
    private WebElement phone;

    @FindBy(css = "select[title='billingcountry']")
    private WebElement country;

    @FindBy(css = "input[title='billingcity']")
    private WebElement city;

    @FindBy(css = "input[id='shippingSameBilling']")
    private WebElement shippingSameBillingCheckbox;

    public void setShippingAddressSameAsBilling() {
        waitForElement(shippingSameBillingCheckbox);
        shippingSameBillingCheckbox.click();
    }

    public void purchase() {
        purchase.click();
    }

    public void setUserData(User user) {
        setFirstName(user.getFirstName());
        setLastName(user.getLastName());
        setUndefined(user.getUndefined());
        setPhone(user.getPhone());
    }

    public void setAddressData(User user) {
        setAddress(user.getAddress());
        setCity(user.getCity());
        setCountry(user.getCountry());
    }

    public void setFirstName(String firstName) {
        this.firstName.sendKeys(firstName);
    }

    public void setLastName(String lastName) {
        this.lastName.sendKeys(lastName);
    }

    public void setAddress(String address) {
        this.address.sendKeys(address);
    }

    public void setCity(String city) {
        this.city.sendKeys(city);
    }

    public void setCountry(String countryName) {
        Select countrySelect = new Select(country);
        countrySelect.selectByVisibleText(countryName);
    }

    public void setPhone(String phone) {
        this.phone.sendKeys(phone);
    }

    public void setUndefined(String undefined) {
        this.undefined.sendKeys(undefined);
    }
}
