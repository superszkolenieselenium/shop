package Pages;

import Pages.Base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class ListOfProductsPage extends BasePage {

    @FindBy(className = "wpsc_product_title")
    private List<WebElement> products;

    public ListOfProductsPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void openProduct(String productName) {
        for (WebElement product : products) {
            if (product.getText().equals(productName)) {
                product.click();
                return;
            }
        }
    }
}
