package Pages;

import Models.Order;
import Pages.Base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.math.BigDecimal;

public class ProductPage extends BasePage {

    @FindBy(className = "prodtitle")
    private WebElement productName;

    @FindBy(css = ".currentprice")
    private WebElement productPrice;

    @FindBy(name = "Buy")
    private WebElement addToBasketButton;

    public ProductPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }
    public void addToBasket(Order order){
        order.addNewProduct(getName(), getPrice());
        addToBasketButton.click();
    }

    public String getName(){
        return productName.getText();
    }

    public BigDecimal getPrice(){
        String price = productPrice.getText();
        price = price.substring(1,price.length());
        return new BigDecimal(price);
    }


}
